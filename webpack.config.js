const path = require('path')

module.exports = {
    entry: {
        index: ['babel-polyfill', './src/index.js'],
        cardapio: ['babel-polyfill', './src/cardapio.js'],
        cremes: ['babel-polyfill', './src/cremes.js'],
        conftemp1: ['babel-polyfill', './src/conftemp1.js'],
        conftemp2: ['babel-polyfill', './src/conftemp2.js'],
        confadd: ['babel-polyfill', './src/confadd.js'],
        confirma: ['babel-polyfill', './src/confirma.js'],
        txentrega: ['babel-polyfill', './src/txentrega.js'],
        forms: ['babel-polyfill', './src/forms.js'],
        enviapedidos: ['babel-polyfill', './src/enviapedidos.js']

    },
    output: {
        path: path.resolve(__dirname, 'public/scripts'),
        filename: '[name]-bundle.js'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['env']
                }
            }
        }]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        publicPath: '/scripts/'
    },
    devtool: 'source-map'
}