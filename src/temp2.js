let pedidosTemp2 = []

const loadPedidosTemp2 = function(){
    const pedidosTemp2JSON = sessionStorage.getItem('pedidosTemp2')
    
    if(pedidosTemp2JSON !== null){
        return JSON.parse(pedidosTemp2JSON)
    } else {
        return []
    }
}

const savePedidosTemp2 = function(){
    sessionStorage.setItem('pedidosTemp2', JSON.stringify(pedidosTemp2))
}

//expose orders from module
const getPedidosTemp2 = () => pedidosTemp2

const criaPedidosTemp2 = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosTemp2.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    savePedidosTemp2()
}

const removePedidosTemp2 = (item) => {
    pedidosTemp2.splice(item, 1)
    savePedidosTemp2()
}

const apagatemp2 = () => {
    sessionStorage.removeItem('pedidosTemp2')
}

pedidosTemp2 = loadPedidosTemp2()

export { getPedidosTemp2, criaPedidosTemp2, savePedidosTemp2, removePedidosTemp2, apagatemp2}