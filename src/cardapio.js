import {addCardapio} from './menuHam'
import { apagaTemp1 } from './temp1'
import { apagatemp2 } from './temp2'
import { apagatemp3 } from './temp3'
import { apagatemp4 } from './temp4'

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

addCardapio()
apagaTemp1()
apagatemp2()
apagatemp3()
apagatemp4()

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    window.history.back()
})