import {criaPedidos, getPedidos} from './pedidos'
import { criaPedidosTemp1, getPedidosTemp1, apagaTemp1 } from './temp1'
import { criaPedidosTemp2, getPedidosTemp2 } from './temp2'
import { criaPedidosTemp3 } from './temp3'
import {criaPedidosTemp4, removePedidosTemp4} from './temp4'

let pedidosTemp4 = []

const loadPedidosTemp4 = function(){
    const pedidosTemp4JSON = sessionStorage.getItem('pedidosTemp4')
    
    if(pedidosTemp4JSON !== null){
        return JSON.parse(pedidosTemp4JSON)
    } else {
        return []
    }
}

//cardapio da tela
const cardapio = [{
    nomeHamburguer: 'Açaí 250g',
    descricao: 'Açaí, leite em pó, leite condensado, granola e banana.',
    preco: 6,
    foto: './images/',
    tipo: 'acai'
}, {
    nomeHamburguer: 'Açaí 500g',
    descricao: 'Açaí, leite em pó, leite condensado, granola, M&M, banana, manga, morango e kiwi.',
    preco: 10,
    foto: './images/',
    tipo: 'acai'
}, {
    nomeHamburguer: 'Açaí 750g',
    descricao: 'Açaí, leite em pó, leite condensado, granola, M&M, sucrilhos, banana, manga, morango e kiwi.',
    preco: 15,
    foto: './images/',
    tipo: 'acai'
}, {
    nomeHamburguer: 'Açaí 1kg',
    descricao: 'Açaí, leite em pó, leite condensado, granola, M&M, sucrilhos, chocoball, banana, manga, morango e kiwi.',
    preco: 22,
    foto: './images/',
    tipo: 'acai'
}, {
    nomeHamburguer: 'Açaí Barca',
    descricao: 'Açaí, leite em pó, leite condensado, granola, M&M, sucrilhos, chocoball, banana, manga, morango e kiwi.',
    preco: 40,
    foto: './images/',
    tipo: 'acai'
}, {
    nomeHamburguer: 'Creme 250g',
    descricao: 'Açaí, leite em pó, leite condensado, granola e banana.',
    preco: 6,
    foto: './images/',
    tipo: 'creme'
}, {
    nomeHamburguer: 'Creme 500g',
    descricao: 'Creme, leite em pó, leite condensado, granola, M&M, banana, manga, morango e kiwi.',
    preco: 10,
    foto: './images/',
    tipo: 'creme'
}, {
    nomeHamburguer: 'Creme 750g',
    descricao: 'Creme, leite em pó, leite condensado, granola, M&M, sucrilhos, banana, manga, morango e kiwi.',
    preco: 15,
    foto: './images/',
    tipo: 'creme'
}, {
    nomeHamburguer: 'Creme 1kg',
    descricao: 'Creme, leite em pó, leite condensado, granola, M&M, sucrilhos, chocoball, banana, manga, morango e kiwi.',
    preco: 22,
    foto: './images/',
    tipo: 'creme'
}/*, {
    nomeHamburguer: 'Ninho com Oreo',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}*/, {
    nomeHamburguer: 'Amendoim',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}, {
    nomeHamburguer: 'Sonho de Valsa',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}/*, {
    nomeHamburguer: 'Cupuaçu Trufado',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}*/, {
    nomeHamburguer: 'Cupuaçu',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}/*, {
    nomeHamburguer: 'Cupuaçu com Morango',
    descricao: '',
    preco: 0,
    foto: './images/',
    tipo: 'sabores'
}*/, {
    nomeHamburguer: 'Sucrilhos',
    descricao: '',
    preco: 1,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Morango',
    descricao: '',
    preco: 2,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Banana',
    descricao: '',
    preco: 1,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Kiwi',
    descricao: '',
    preco: 2,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Manga',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Leite em Pó',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Leite Condensado',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Granola',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'M&M',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Chocoball',
    descricao: '',
    preco: 1,
    foto: './images/',
    tipo: 'adicionais'
}, {
    nomeHamburguer: 'Farinha de Amendoim',
    descricao: '',
    preco: 1.5,
    foto: './images/',
    tipo: 'adicionais'
}]

const addElements = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    if(lanche.tipo === 'sabores'){
        btn.textContent = 'ESCOLHER'
    }else{
        btn.textContent = 'PEDIR'
    }
    
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    
    //divMain.appendChild(imagem)
    select.setAttribute('style', 'grid-column: 1;')
    if(lanche.tipo !== 'sabores'){
        divMain.appendChild(descrito)
        divMain.appendChild(select)
        select.appendChild(opt)
        select.appendChild(opt2)
        select.appendChild(opt3)
        divMain.appendChild(subValor)
    }

    if(lanche.tipo === 'adicionais'){
        select.setAttribute('style', 'display: none;')
        descrito.setAttribute('style', 'display: none;')
    }


    
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const qt = parseFloat(select.value)
        const prodt = lanche.nomeHamburguer
        const price = lanche.preco
        const tp = lanche.tipo
        if(lanche.tipo === 'acai'){
            criaPedidosTemp2(qt, prodt, price, tp)
            location.assign('./conftemp2.html')
        }else if(lanche.tipo === 'sabores' && getPedidosTemp1().filter((x) => x.taxa === 'acai').length < 1 && getPedidosTemp1()[0].produto.includes('(') == false){
             criaPedidosTemp2(getPedidosTemp1()[0].qtd, getPedidosTemp1()[0].produto + ' (' + prodt + ') ', getPedidosTemp1()[0].preco, getPedidosTemp1()[0].taxa)
             location.assign('./conftemp2.html')
        }else if(lanche.tipo === 'sabores' && getPedidosTemp1().filter((x) => x.taxa === 'acai').length > 0){
            criaPedidosTemp3(getPedidosTemp1()[0].qtd, getPedidosTemp1()[0].produto + ' + (' + prodt + ') ', getPedidosTemp1()[0].preco)
            apagaTemp1()
            location.assign('./confadd.html')
        }else if(lanche.tipo === 'sabores' && getPedidosTemp1().filter((x) => x.taxa === 'acai').length < 1 && getPedidosTemp1()[0].produto.includes('(') == true){
            criaPedidosTemp3(getPedidosTemp1()[0].qtd, getPedidosTemp1()[0].produto + ' + (' + prodt + ') ', getPedidosTemp1()[0].preco)
            apagaTemp1()
            location.assign('./confadd.html')
        }else if(lanche.tipo === 'adicionais'){
            criaPedidosTemp4(qt, prodt, price)
            location.reload()
        }
        else{
            criaPedidosTemp1(qt, prodt, price, tp)
            location.assign('./conftemp1.html')
        }
    
        
    })

    return divMain
}

const addElementsEscolhidos = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const subValor = document.createElement('div')
    const btn = document.createElement('button')

    divMain.setAttribute('class', 'container')
    
    titulo.textContent = lanche.produto
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    subValor.setAttribute('class', 'subtotalValor')
    subValor.textContent = 'R$ ' + lanche.preco.toFixed(2).replace('.', ',')
    btn.textContent = 'REMOVER'
    btn.setAttribute('class', 'btnPedir')
    
    
    divMain.appendChild(titulo)
    //divMain.appendChild(descrito)
    //divMain.appendChild(imagem)
    //divMain.appendChild(select)
    //select.appendChild(opt)
    //select.appendChild(opt2)
    //select.appendChild(opt3)
    divMain.appendChild(subValor)
    btn.setAttribute('style', 'font-size: 9px; background-color: red; font-weight: bold;')
    divMain.appendChild(btn)

    select.addEventListener('change', (e) => {
        subValor.textContent = `R$ ${e.target.value * lanche.preco},00`
        subValor.setAttribute('style', 'color: yellow;')        
    })
    
    
    btn.addEventListener('click', () => {
        const a = pedidosTemp4.indexOf(lanche)
        removePedidosTemp4(a)
        location.reload()
    })

    return divMain
}

const addCardapio = () => {
    cardapio.filter((x) => x.tipo === 'acai').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docTable').appendChild(hrEl)
        document.querySelector('#docTable').appendChild(addElements(lanche))
        
    })
}

const addCardapioCremes = () => {
    cardapio.filter((x) => x.tipo === 'creme').forEach((lanche) => {
        const hrEl = document.createElement('hr')
        document.querySelector('#docCremes').appendChild(hrEl)
        document.querySelector('#docCremes').appendChild(addElements(lanche))
        
    })
}

const addCardapioSabores = () => {
    cardapio.filter((x) => x.tipo === 'sabores').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docSabores').appendChild(hrEl)
        document.querySelector('#docSabores').appendChild(addElements(lanche))
        
    })
}

const addCardapioAdicionais = () => {
    cardapio.filter((x) => x.tipo === 'adicionais').forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docSabores').appendChild(hrEl)
        document.querySelector('#adicionaisDom').appendChild(addElements(lanche))
        
    })
}

const addCardapioAdicionaisEscolhidos = () => {
    pedidosTemp4.forEach((lanche) => {
        // const hrEl = document.createElement('hr')
        // document.querySelector('#docSabores').appendChild(hrEl)
        document.querySelector('#addescolhidos').appendChild(addElementsEscolhidos(lanche))
        
    })
}

pedidosTemp4 = loadPedidosTemp4()

export {addCardapio, addCardapioCremes, addCardapioSabores, addCardapioAdicionais, addCardapioAdicionaisEscolhidos, addElements}