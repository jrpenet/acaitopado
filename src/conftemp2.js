import {criaPedidosTemp3, getPedidosTemp3, apagatemp3} from './temp3'
import {getPedidosTemp2, apagatemp2} from './temp2'
import { apagaTemp1, criaPedidosTemp1, getPedidosTemp1 } from './temp1'
import { getPedidosTemp4, apagatemp4} from './temp4'
import { criaPedidos } from './pedidos'

(function() {
    if( window.sessionStorage ) {
 
       if( !sessionStorage.getItem( 'firstLoad' ) ) {
          sessionStorage[ 'firstLoad' ] = true;
          window.location.reload();
 
       } else {
          sessionStorage.removeItem( 'firstLoad' );
       }
    }
 })(); 

apagaTemp1()

// console.log(getPedidosTemp1()[0])
// console.log(getPedidosTemp2()[0])
// console.log(getPedidosTemp3()[0])
// console.log(getPedidosTemp4()[0])

if(getPedidosTemp1()[0] == undefined && getPedidosTemp2()[0] == undefined && getPedidosTemp3()[0] == undefined && getPedidosTemp4()[0] == undefined){
    location.assign('./index.html')
}

if(getPedidosTemp2().length > 0){
    const sabor1 = document.querySelector('#mostraescolha')
    sabor1.textContent = getPedidosTemp2()[0].produto
}


const fechapedido = document.querySelector('#fechapedido')
fechapedido.addEventListener('click', () => {
    criaPedidosTemp3(getPedidosTemp2()[0].qtd, getPedidosTemp2()[0].produto, getPedidosTemp2()[0].preco)
    apagatemp2()
    location.assign('./confadd.html')
})

const addsabor = document.querySelector('#addsabor')
addsabor.addEventListener('click', () => {
    criaPedidosTemp1(getPedidosTemp2()[0].qtd, getPedidosTemp2()[0].produto, getPedidosTemp2()[0].preco, getPedidosTemp2()[0].taxa)
    apagatemp2()
    location.assign('./conftemp1.html')
})

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    apagaTemp1()
    apagatemp2()
    apagatemp3()
    apagatemp4()
    location.reload()
})