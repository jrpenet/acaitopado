let pedidosTemp1 = []

const loadPedidosTemp1 = function(){
    const pedidosTemp1JSON = sessionStorage.getItem('pedidosTemp1')
    
    if(pedidosTemp1JSON !== null){
        return JSON.parse(pedidosTemp1JSON)
    } else {
        return []
    }
}

const savePedidosTemp1 = function(){
    sessionStorage.setItem('pedidosTemp1', JSON.stringify(pedidosTemp1))
}

//expose orders from module
const getPedidosTemp1 = () => pedidosTemp1

const criaPedidosTemp1 = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosTemp1.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    savePedidosTemp1()
}

const removePedidosTemp1 = (item) => {
    pedidosTemp1.splice(item, 1)
    savePedidosTemp1()
}

const apagaTemp1 = () => {
    sessionStorage.removeItem('pedidosTemp1')
}

pedidosTemp1 = loadPedidosTemp1()

export { getPedidosTemp1, criaPedidosTemp1, savePedidosTemp1, removePedidosTemp1, apagaTemp1}