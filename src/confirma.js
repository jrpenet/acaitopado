import {getPedidos} from './pedidos'
import {cartTable, insereCart, rodapeCart} from './cart'
import { apagaTemp1 } from './temp1'
import { apagatemp2 } from './temp2'
import { apagatemp3 } from './temp3'
import { apagatemp4 } from './temp4'



cartTable()
insereCart()
rodapeCart()

apagaTemp1()
apagatemp2()
apagatemp3()
apagatemp4()


document.querySelector('.btn-comprarMais').addEventListener('click', (e) => {
    e.preventDefault()
    location.assign('./index.html')
})

document.querySelector('#finalizarPedido').addEventListener('click', (e) => {
    e.preventDefault()
    if(getPedidos().map((x)=> x.taxa).includes('taxa')){
        location.assign('./forms.html')
    }else{
        location.assign('./txentrega.html')
    }
})