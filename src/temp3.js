let pedidosTemp3 = []

const loadPedidosTemp3 = function(){
    const pedidosTemp3JSON = sessionStorage.getItem('pedidosTemp3')
    
    if(pedidosTemp3JSON !== null){
        return JSON.parse(pedidosTemp3JSON)
    } else {
        return []
    }
}

const savePedidosTemp3 = function(){
    sessionStorage.setItem('pedidosTemp3', JSON.stringify(pedidosTemp3))
}

//expose orders from module
const getPedidosTemp3 = () => pedidosTemp3

const criaPedidosTemp3 = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosTemp3.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    savePedidosTemp3()
}

const removePedidosTemp3 = (item) => {
    pedidosTemp3.splice(item, 1)
    savePedidosTemp3()
}

const apagatemp3 = () => {
    sessionStorage.removeItem('pedidosTemp3')
}

pedidosTemp3 = loadPedidosTemp3()

export { getPedidosTemp3, criaPedidosTemp3, savePedidosTemp3, removePedidosTemp3, apagatemp3}