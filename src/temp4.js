let pedidosTemp4 = []

const loadPedidosTemp4 = function(){
    const pedidosTemp4JSON = sessionStorage.getItem('pedidosTemp4')
    
    if(pedidosTemp4JSON !== null){
        return JSON.parse(pedidosTemp4JSON)
    } else {
        return []
    }
}

const savePedidosTemp4 = function(){
    sessionStorage.setItem('pedidosTemp4', JSON.stringify(pedidosTemp4))
}

//expose orders from module
const getPedidosTemp4 = () => pedidosTemp4

const criaPedidosTemp4 = (select, hamb, precoProduto, tx, bairro) =>{
    
    pedidosTemp4.push({
        qtd: select,
        produto: hamb,
        preco: precoProduto,
        subt: select * precoProduto,
        taxa: tx,
        nomeDoBairro: bairro
    })
    savePedidosTemp4()
}

const removePedidosTemp4 = (item) => {
    pedidosTemp4.splice(item, 1)
    savePedidosTemp4()
}

const apagatemp4 = () => {
    sessionStorage.removeItem('pedidosTemp4')
}

pedidosTemp4 = loadPedidosTemp4()

export { getPedidosTemp4, criaPedidosTemp4, savePedidosTemp4, removePedidosTemp4, apagatemp4}