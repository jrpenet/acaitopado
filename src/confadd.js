import {addCardapioAdicionais, addCardapioAdicionaisEscolhidos} from './menuHam'
import {getPedidosTemp3, apagatemp3} from './temp3'
import { criaPedidos } from './pedidos'
import { getPedidosTemp4, apagatemp4 } from './temp4'
import { apagaTemp1 } from './temp1'
import { apagatemp2 } from './temp2'

addCardapioAdicionais()

const sabor1 = document.querySelector('#mostraescolha')
sabor1.textContent = getPedidosTemp3()[0].produto

if(getPedidosTemp4().length > 0){
    const adicionei = document.createElement('h2')
    adicionei.textContent = 'Quero adicionar'
    document.querySelector('#addescolhidos').appendChild(adicionei)
    addCardapioAdicionaisEscolhidos()
}

document.querySelector('.btn-goBack').addEventListener('click', (e) => {
    e.preventDefault()
    apagaTemp1()
    apagatemp2()
    apagatemp3()
    apagatemp4()
    location.assign('./index.html')
})

document.querySelector('.confirmaPedidoeAdd').addEventListener('click', (e) => {
    e.preventDefault()
    const infoplus = document.querySelector('#maisinfo')
    console.log(infoplus.value)
    
    criaPedidos(getPedidosTemp3()[0].qtd, getPedidosTemp3()[0].produto + ' Obs: '+ infoplus.value, getPedidosTemp3()[0].preco)
    getPedidosTemp4().map((x) => criaPedidos(x.qtd, 'Adicional: ('+ getPedidosTemp3()[0].produto + ') ' + x.produto, x.preco))
    apagatemp3()
    apagatemp4()
    location.assign('./confirma.html')
})